/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.GroupEvalListVO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class GradeDAO {
        
    static public Long addList(GroupEvalListVO evalList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(evalList);
        sesion.getTransaction().commit();
        return evalList.getId();
    }
    static public void updateList(GroupEvalListVO evalList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(evalList);
        sesion.getTransaction().commit();
    }
    static public List<GroupEvalListVO> getList(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupEvalListVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupEvalListVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
