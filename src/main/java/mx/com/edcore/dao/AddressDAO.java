/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.AddressBookVO;
import mx.com.edcore.vo.CountryVO;
import mx.com.edcore.vo.CurrencyVO;
import mx.com.edcore.vo.LocationVO;
import mx.com.edcore.vo.StateVO;
import mx.com.edcore.vo.TownVO;
import java.util.List;
import mx.com.edcore.view.VIW056;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class AddressDAO {
    

    static public List<CurrencyVO> getCurrencies(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(CurrencyVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<CurrencyVO>  list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public List<CountryVO> getCuntries(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(CountryVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<CountryVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addState(StateVO state){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(state);
        sesion.getTransaction().commit();
        return state.getId();
    }
    static public void updateState(StateVO state){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(state);
        sesion.getTransaction().commit();
    }
    static public List<StateVO> getStates(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(StateVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<StateVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addTown(TownVO town){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(town);
        sesion.getTransaction().commit();
        return town.getId();
    }
    static public void updateTown(TownVO town){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(town);
        sesion.getTransaction().commit();
    }
    static public List<TownVO> getTowns(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(TownVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<TownVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addLocation(LocationVO location){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(location);
        sesion.getTransaction().commit();
        return location.getId();
    }
    static public void updateLocation(LocationVO location){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(location);
        sesion.getTransaction().commit();
    }
    static public List<LocationVO> getLocations(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(LocationVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<LocationVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addAddress(AddressBookVO addressBook){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(addressBook);
        sesion.getTransaction().commit();
        return addressBook.getId();
    }
    static public void updateAddress(AddressBookVO addressBook){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(addressBook);
        sesion.getTransaction().commit();
    }
    static public List<VIW056> getAddress(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW056.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW056> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
