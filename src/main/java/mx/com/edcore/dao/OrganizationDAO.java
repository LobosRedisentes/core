/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import java.util.List;
import mx.com.edcore.view.VIW059;
import mx.com.edcore.vo.OfficeVO;
import mx.com.edcore.vo.WorkStationVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OrganizationDAO {
        
    static public void updateOffice(OfficeVO office){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(office);
        sesion.getTransaction().commit();
    }
    static public List<VIW059> getOffices(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW059.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW059> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addWorkstation(WorkStationVO workstation){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(workstation);
        sesion.getTransaction().commit();
        return workstation.getId();
    }
    static public void updateWorkstation(WorkStationVO workstation){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(workstation);
        sesion.getTransaction().commit();
    }
    static public List<WorkStationVO> getWorkstations(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(WorkStationVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<WorkStationVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
