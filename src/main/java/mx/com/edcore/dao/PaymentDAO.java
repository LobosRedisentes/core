/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.DiscountVO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PaymentDAO {
    
    
    static public Long addDiscount(DiscountVO discount){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(discount);
        sesion.getTransaction().commit();
        return discount.getId();
    }
    static public void updateDiscount(DiscountVO discount){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(discount);
        sesion.getTransaction().commit();
    }
    static public List<DiscountVO> getDiscounts(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(DiscountVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<DiscountVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
