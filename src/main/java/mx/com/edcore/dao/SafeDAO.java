/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.vo.AccountVO;
import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.OperationVO;
import mx.com.edcore.vo.LockVO;
import mx.com.edcore.vo.LogVO;
import mx.com.edcore.vo.PrivilegeVO;
import mx.com.edcore.vo.RolVO;
import mx.com.edcore.vo.SessionVO;
import java.util.List;
import mx.com.edcore.view.VIW028;
import mx.com.edcore.view.VIW048;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class SafeDAO {
    
    static public Long addAccount(AccountVO account){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(account);
        sesion.getTransaction().commit();
        return account.getId();
    }
    static public void updateAccount(AccountVO account){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(account);
        sesion.getTransaction().commit();
    }
    static public List<VIW028> getAccounts(String sql, Integer displaylength, Integer displaystart) {
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW028.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW028> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addRol(RolVO rol){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(rol);
        sesion.getTransaction().commit();
        return rol.getId();
    }
    static public void updateRol(RolVO rol){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(rol);
        sesion.getTransaction().commit();
    }
    static public List<RolVO> getRoles(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(RolVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<RolVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addPrivilege(PrivilegeVO privilege){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(privilege);
        sesion.getTransaction().commit();
        return privilege.getId();
    }
    static public void updatePrivilege(PrivilegeVO privilege){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(privilege);
        sesion.getTransaction().commit();
    }
    static public List<VIW048> getPrivileges(String sql, Integer displaylength, Integer displaystart) {
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW048.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW048> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addSession(SessionVO session){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(session);
        sesion.getTransaction().commit();
        return session.getId();
    }
    static public void updateSession(SessionVO session){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(session);
        sesion.getTransaction().commit();
    }
    static public List<SessionVO> getSessions(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(SessionVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<SessionVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addLock(LockVO lock){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(lock);
        sesion.getTransaction().commit();
        return lock.getId();
    }
    static public void updateLock(LockVO lock){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(lock);
        sesion.getTransaction().commit();
    }
    static public List<LockVO> getLocks(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(LockVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<LockVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addLog(LogVO log){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(log);
        sesion.getTransaction().commit();
        return log.getId();
    }
    static public List<LogVO> getLog(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(LogVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<LogVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public List<OperationVO> getOperations(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(OperationVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<OperationVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
