/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.AdministrativeVO;
import mx.com.edcore.vo.ApplicantVO;
import mx.com.edcore.vo.ProfessorProfileVO;
import mx.com.edcore.vo.ProfessorVO;
import mx.com.edcore.vo.StudentVO;
import mx.com.edcore.vo.TutorVO;
import mx.com.edcore.vo.UserPhotoVO;
import mx.com.edcore.vo.UserVO;
import java.util.List;
import mx.com.edcore.view.VIW058;
import mx.com.edcore.vo.AccountVO;
import mx.com.edcore.vo.AddressBookVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class UserDAO {
        
    static public Long addUser(UserVO user, AddressBookVO addrBook,AccountVO account){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(addrBook);
        user.setAdressBook(addrBook.getId());
        sesion.save(user);
        account.setUser(user.getId());
        sesion.save(account);
        sesion.getTransaction().commit();
        return user.getId();
    }
    static public Long addUser(UserVO user, AddressBookVO addrBook){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(addrBook);
        user.setAdressBook(addrBook.getId());
        sesion.save(user);
        sesion.getTransaction().commit();
        return user.getId();
    }
    static public void updateUser(UserVO user, AddressBookVO addrBook){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(addrBook);
        sesion.update(user);
        sesion.getTransaction().commit();
    }
    static public List<VIW058> getUsers(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW058.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW058> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public void updatePhoto(UserPhotoVO photo){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(photo);
        sesion.getTransaction().commit();
    }
    static public UserPhotoVO getPhoto(Long id){
        UserPhotoVO obj;
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createQuery("FROM UserPhotoVO WHERE id = "+id);
        obj = (UserPhotoVO) query.uniqueResult();
        sesion.getTransaction().commit();
        return obj;
    }
    static public Long addStudent(StudentVO student){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(student);
        sesion.getTransaction().commit();
        return student.getId();
    }
    static public void updateStudent(StudentVO student){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(student);
        sesion.getTransaction().commit();
    }
    static public List<StudentVO> getStudents(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(StudentVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<StudentVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addProfessor(ProfessorVO professor){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(professor);
        sesion.getTransaction().commit();
        return professor.getId();
    }
    static public void updateProfessor(ProfessorVO professor){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(professor);
        sesion.getTransaction().commit();
    }
    static public List<ProfessorVO> getProfessors(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ProfessorVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ProfessorVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addProfessorProfile(ProfessorProfileVO professorProfile){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(professorProfile);
        sesion.getTransaction().commit();
        return professorProfile.getId();
    }
    static public void updateProfessorProfile(ProfessorProfileVO professorProfile){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(professorProfile);
        sesion.getTransaction().commit();
    }
    static public List<ProfessorProfileVO> getProfessorProfiles(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ProfessorProfileVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ProfessorProfileVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addApplicant(ApplicantVO applicant){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(applicant);
        sesion.getTransaction().commit();
        return applicant.getId();
    }
    static public void updateApplicant(ApplicantVO applicant){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(applicant);
        sesion.getTransaction().commit();
    }
    static public List<ApplicantVO> getApplicants(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ApplicantVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ApplicantVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addAdministrative(AdministrativeVO administrative){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(administrative);
        sesion.getTransaction().commit();
        return administrative.getId();
    }
    static public void updateAdministrative(AdministrativeVO administrative){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(administrative);
        sesion.getTransaction().commit();
    }
    static public List<AdministrativeVO> getAdministratives(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(AdministrativeVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<AdministrativeVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addTutor(TutorVO tutor){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(tutor);
        sesion.getTransaction().commit();
        return tutor.getId();
    }
    static public void updateTutor(TutorVO tutor){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(tutor);
        sesion.getTransaction().commit();
    }
    static public List<TutorVO> getTutors(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(TutorVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<TutorVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
