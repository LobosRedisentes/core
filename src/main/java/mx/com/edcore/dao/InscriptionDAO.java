/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.AcceptedListVO;
import mx.com.edcore.vo.ApplicationVO;
import mx.com.edcore.vo.CodeVO;
import mx.com.edcore.vo.ExamProgramVO;
import mx.com.edcore.vo.ExamRoomVO;
import mx.com.edcore.vo.ExamVO;
import mx.com.edcore.vo.InscriptionListVO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class InscriptionDAO {
    
    static public Long addApplication(ApplicationVO application){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(application);
        sesion.getTransaction().commit();
        return application.getId();
    }
    static public void updateApplication(ApplicationVO application){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(application);
        sesion.getTransaction().commit();
    }
    static public List<ApplicationVO> getApplications(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ApplicationVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ApplicationVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addCode(CodeVO code){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(code);
        sesion.getTransaction().commit();
        return code.getId();
    }
    static public void updateCode(CodeVO code){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(code);
        sesion.getTransaction().commit();
    }
    static public List<CodeVO> getCodes(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(CodeVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<CodeVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addExam(ExamVO exam){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(exam);
        sesion.getTransaction().commit();
        return exam.getId();
    }
    static public void updateExam(ExamVO exam){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(exam);
        sesion.getTransaction().commit();
    }
    static public List<ExamVO> getExams(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ExamVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ExamVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addExamRoom(ExamRoomVO examRoom){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(examRoom);
        sesion.getTransaction().commit();
        return examRoom.getId();
    }
    static public void updateExamRoom(ExamRoomVO examRoom){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(examRoom);
        sesion.getTransaction().commit();
    }
    static public List<ExamRoomVO> getExamRooms(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ExamRoomVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ExamRoomVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addExamProgram(ExamProgramVO examProgram){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(examProgram);
        sesion.getTransaction().commit();
        return examProgram.getId();
    }
    static public void updateExamProgram(ExamProgramVO examProgram){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(examProgram);
        sesion.getTransaction().commit();
    }
    static public List<ExamProgramVO> getExamPrograms(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ExamProgramVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ExamProgramVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addAccepted(AcceptedListVO acceptedList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(acceptedList);
        sesion.getTransaction().commit();
        return acceptedList.getId();
    }
    static public void updateAccepted(AcceptedListVO acceptedList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(acceptedList);
        sesion.getTransaction().commit();
    }
    static public List<AcceptedListVO> getAccepted(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(AcceptedListVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<AcceptedListVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addList(InscriptionListVO inscriptionList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(inscriptionList);
        sesion.getTransaction().commit();
        return inscriptionList.getId();
    }
    static public void updateList(InscriptionListVO inscriptionList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(inscriptionList);
        sesion.getTransaction().commit();
    }
    static public List<InscriptionListVO> getList(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(InscriptionListVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<InscriptionListVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
