/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.PlanVO;
import mx.com.edcore.vo.ProgramVO;
import mx.com.edcore.vo.SubjectVO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ProgramDAO {
    
    static public Long addProgram(ProgramVO program){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(program);
        sesion.getTransaction().commit();
        return program.getId();
    }
    static public void updateProgram(ProgramVO program){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(program);
        sesion.getTransaction().commit();
    }
    static public List<ProgramVO> getPrograms(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ProgramVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ProgramVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addPlan(PlanVO plan){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(plan);
        sesion.getTransaction().commit();
        return plan.getId();
    }
    static public void updatePlan(PlanVO plan){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(plan);
        sesion.getTransaction().commit();
    }
    static public List<PlanVO> getPlans(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(PlanVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<PlanVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }    
    static public Long addSubject(SubjectVO subject){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(subject);
        sesion.getTransaction().commit();
        return subject.getId();
    }
    static public void updateSubject(SubjectVO subject){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(subject);
        sesion.getTransaction().commit();
    }
    static public List<SubjectVO> getSubjects(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(SubjectVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<SubjectVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
