/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.MessageVO;
import mx.com.edcore.vo.NoteVO;
import java.util.List;
import mx.com.edcore.view.VIW046;
import mx.com.edcore.vo.MessageAttachedVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PublicationDAO {
    
    static public Long addMessage(MessageVO msn){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(msn);
        sesion.getTransaction().commit();
        return msn.getId();
    }
    static public Long addMessage(MessageVO msn, List<MessageAttachedVO> attached){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(msn);
        for (MessageAttachedVO obj : attached) {
            obj.setMessage(msn.getId());
            sesion.save(obj);
        }
        sesion.getTransaction().commit();
        return msn.getId();
    }
    static public void updateMessage(MessageVO msn){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(msn);
        sesion.getTransaction().commit();
    }
    static public List<VIW046> getMessages(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW046.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW046> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addNote(NoteVO note){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(note);
        sesion.getTransaction().commit();
        return note.getId();
    }
    static public void updateNote(NoteVO note){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(note);
        sesion.getTransaction().commit();
    }
    static public List<NoteVO> getNotes(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(NoteVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<NoteVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
