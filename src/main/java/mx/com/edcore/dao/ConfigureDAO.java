/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import java.util.List;
import mx.com.edcore.vo.ParameterVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ConfigureDAO {
        
    static public void updateParameter(ParameterVO parameter){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(parameter);
        sesion.getTransaction().commit();
    }
    static public List<ParameterVO> getParameters(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ParameterVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ParameterVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
