/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.GroupInscriptionVO;
import mx.com.edcore.vo.GroupListVO;
import mx.com.edcore.vo.GroupPackageVO;
import mx.com.edcore.vo.GroupScheduleVO;
import mx.com.edcore.vo.GroupVO;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class GroupDAO {
        
    static public Long addGroup(GroupVO group){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(group);
        sesion.getTransaction().commit();
        return group.getId();
    }
    static public void updateGroup(GroupVO group){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(group);
        sesion.getTransaction().commit();
    }
    static public List<GroupVO> getGroups(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addPackage(GroupPackageVO groupPackage){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(groupPackage);
        sesion.getTransaction().commit();
        return groupPackage.getId();
    }
    static public void updatePackage(GroupPackageVO groupPackage){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(groupPackage);
        sesion.getTransaction().commit();
    }
    static public List<GroupPackageVO> getPackages(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupPackageVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupPackageVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addInscription(GroupInscriptionVO groupInscription){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(groupInscription);
        sesion.getTransaction().commit();
        return groupInscription.getId();
    }
    static public void updateInscription(GroupInscriptionVO groupInscription){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(groupInscription);
        sesion.getTransaction().commit();
    }
    static public List<GroupInscriptionVO> getInscriptions(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupInscriptionVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupInscriptionVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addList(GroupListVO groupList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(groupList);
        sesion.getTransaction().commit();
        return groupList.getId();
    }
    static public void updateList(GroupListVO groupList){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(groupList);
        sesion.getTransaction().commit();
    }
    static public List<GroupListVO> getList(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupListVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupListVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addSchedule(GroupScheduleVO groupSchedule){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(groupSchedule);
        sesion.getTransaction().commit();
        return groupSchedule.getId();
    }
    static public void updateSchedule(GroupScheduleVO groupSchedule){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(groupSchedule);
        sesion.getTransaction().commit();
   }
    static public List<GroupScheduleVO> getSchedules(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(GroupScheduleVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<GroupScheduleVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
