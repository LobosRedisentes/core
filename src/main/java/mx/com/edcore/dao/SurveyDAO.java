/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import java.util.List;
import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.ApplicantSurveyVO;
import mx.com.edcore.vo.SurveyHealthVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class SurveyDAO {
    
    static public Long addHealth(SurveyHealthVO survey){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(survey);
        sesion.getTransaction().commit();
        return survey.getId();
    }
    static public void updateHealth(SurveyHealthVO survey){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(survey);
        sesion.getTransaction().commit();
    }
    static public List<SurveyHealthVO> getHealths(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(SurveyHealthVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<SurveyHealthVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
    static public Long addApplicant(ApplicantSurveyVO applicantSurvey){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(applicantSurvey);
        sesion.getTransaction().commit();
        return applicantSurvey.getId();
    }
    static public void updateApplicant(ApplicantSurveyVO applicantSurvey){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(applicantSurvey);
        sesion.getTransaction().commit();
    }
    static public List<ApplicantSurveyVO> getApplicant(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(ApplicantSurveyVO.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<ApplicantSurveyVO> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
