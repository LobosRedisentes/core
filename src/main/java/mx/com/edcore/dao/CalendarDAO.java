/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.dao;

import mx.com.edcore.util.HibernateUtil;
import mx.com.edcore.vo.CalendarVO;
import java.util.List;
import mx.com.edcore.view.VIW055;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class CalendarDAO {
    
    static public Long addCalendar(CalendarVO calendar){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.save(calendar);
        sesion.getTransaction().commit();
        return calendar.getId();
    }
    static public void updateCalendar(CalendarVO calendar){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.update(calendar);
        sesion.getTransaction().commit();
    }
    static public List<VIW055> getCalendars(String sql, Integer displaylength, Integer displaystart){
        SessionFactory sf = HibernateUtil.getSessionFactory();
        Session sesion = sf.getCurrentSession();
        sesion.getTransaction().begin();
        Query query = sesion.createSQLQuery(sql).addEntity(VIW055.class);
        query.setMaxResults(displaylength);
        query.setFirstResult(displaystart);
        List<VIW055> list = query.list();
        sesion.getTransaction().commit();
        return list;
    }
}
