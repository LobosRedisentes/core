/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*   Hugo Alejandro Armenta Briseño
*   04/05/2015
*   ScheduleDays - POJO
**/
@Entity
@Table(name="ScheduleDays")
public class DayVO implements java.io.Serializable{
    
    private Long id;
    private String abreviation;
    private String name;
    
    public DayVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="sch_IdDay")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }
    @Column(name="abreviation")
    public String getAbreviation() {
        return abreviation;
    }
    public void setAbreviation(String abreviation){
        this.abreviation = abreviation;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
}
