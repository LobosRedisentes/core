package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Organizational Rooms Table
 * @author Ricardo Romo Ramírez
 * @version 1.0.0
 */
@Entity
@Table(name="OrgRooms")
public class RoomVO implements java.io.Serializable{
    
    private Long id;
    private Long module;
    private String code;
    private String name;
    private Long maxCapacity;
    private String type;
    
    public RoomVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="org_IdRoom")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    @Column(name="org_IdModule")
    public Long getModule() {
        return module;
    }
    public void setModule(Long module){
        this.module = module;
    }
    
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="maxCapacity")
    public Long getMaxCapacity() {
        return maxCapacity;
    }
    public void setMaxCapacity(Long maxCapacity){
        this.maxCapacity = maxCapacity;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
}
