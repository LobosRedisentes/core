/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProgPlan")
public class PlanVO implements Serializable {
    
    private Long id;
    private Long program;
    private String code;
    private String officialCode;
    private Long credits;
    private Long credReqServ;
    private Long loadMaxServ;
    private Long credReqRes;
    private Long loadMaxRes;
    private Long loadMinCred;
    private Long loadMaxCred;
    private Long loadMaxSpecial;
    private Long hrsCrossing;
    private Long totalSubjects;
    private String type;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="prog_IdPlan")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="prog_IdProgram")
    public Long getProgram() {
        return program;
    }
    public void setProgram(Long program) {
        this.program = program;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Column(name="officialCode")
    public String getOfficialCode() {
        return officialCode;
    }
    public void setOfficialCode(String officialCode) {
        this.officialCode = officialCode;
    }
    @Column(name="credits")
    public Long getCredits() {
        return credits;
    }
    public void setCredits(Long credits) {
        this.credits = credits;
    }
    @Column(name="credReqServ")
    public Long getCredReqServ() {
        return credReqServ;
    }
    public void setCredReqServ(Long credReqServ) {
        this.credReqServ = credReqServ;
    }
    @Column(name="loadMaxServ")
    public Long getLoadMaxServ() {
        return loadMaxServ;
    }
    public void setLoadMaxServ(Long loadMaxServ) {
        this.loadMaxServ = loadMaxServ;
    }
    @Column(name="credReqRes")
    public Long getCredReqRes() {
        return credReqRes;
    }
    public void setCredReqRes(Long credReqRes) {
        this.credReqRes = credReqRes;
    }
    @Column(name="loadMaxRes")
    public Long getLoadMaxRes() {
        return loadMaxRes;
    }
    public void setLoadMaxRes(Long loadMaxRes) {
        this.loadMaxRes = loadMaxRes;
    }
    @Column(name="loadMinCred")
    public Long getLoadMinCred() {
        return loadMinCred;
    }
    public void setLoadMinCred(Long loadMinCred) {
        this.loadMinCred = loadMinCred;
    }
    @Column(name="loadMaxCred")
    public Long getLoadMaxCred() {
        return loadMaxCred;
    }
    public void setLoadMaxCred(Long loadMaxCred) {
        this.loadMaxCred = loadMaxCred;
    }
    @Column(name="loadMaxSpecial")
    public Long getLoadMaxSpecial() {
        return loadMaxSpecial;
    }
    public void setLoadMaxSpecial(Long loadMaxSpecial) {
        this.loadMaxSpecial = loadMaxSpecial;
    }
    @Column(name="hrsCrossing")
    public Long getHrsCrossing() {
        return hrsCrossing;
    }
    public void setHrsCrossing(Long hrsCrossing) {
        this.hrsCrossing = hrsCrossing;
    }
    @Column(name="totalSubjects")
    public Long getTotalSubjects() {
        return totalSubjects;
    }
    public void setTotalSubjects(Long totalSubjects) {
        this.totalSubjects = totalSubjects;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
