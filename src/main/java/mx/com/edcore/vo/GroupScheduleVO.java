package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Schedules per group table
 * @author Ricardo Romo Ramírez
 * @version 1.0.0
 */
@Entity
@Table(name="GroupSchedule")
public class GroupScheduleVO implements java.io.Serializable{
    
    private Long id;
    private Long groupSubject;
    private Long room;
    private Long day;
    private Long hourIn;
    private Long hourOut;
    
    public GroupScheduleVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdGroupSchedule")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }
    
    @Column(name="group_IdGroupSubject")
    public Long getGroupSubject() {
        return this.groupSubject;
    }
    public void setGroupSubject(Long groupSubject){
        this.groupSubject = groupSubject;
    }
    @Column(name="org_IdRoom")
    public Long getRoom() {
        return room;
    }
    public void setRoom(Long room){
        this.room = room;
    }
    @Column(name="sch_IdDay")
    public Long getDay() {
        return day;
    }
    public void setDay(Long day){
        this.day = day;
    }
    
    @Column(name="sch_IdHourIn")
    public Long getHourIn() {
        return this.hourIn;
    }
    public void setHourIn(Long hourIn){
        this.hourIn = hourIn;
    }
    @Column(name="sch_IdHourOut")
    public Long getHourOut() {
        return hourOut;
    }
    public void setHourOut(Long hourOut){
        this.hourOut = hourOut;
    }
}
