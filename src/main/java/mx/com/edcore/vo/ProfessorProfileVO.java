/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="UserProfessorProfile")
public class ProfessorProfileVO {
    
    private Long id;
    private Long professor;
    private Long subject;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_IdProfessorProfile")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="user_IdProfessor")
    public Long getProfessor() {
        return professor;
    }
    public void setProfessor(Long professor) {
        this.professor = professor;
    }

    @Column(name="prog_IdSubject")
    public Long getSubject() {
        return subject;
    }
    public void setSubject(Long subject) {
        this.subject = subject;
    }
}
