/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de aspirantes
 * @author Gabriel Cisneros Landeros
 * @version 1.0.0
 */
@Entity
@Table(name="UserProfessors")
public class ProfessorVO implements java.io.Serializable{
    
    private Long id;
    private Long user;
    private String code;
    private Long department;
    private String professionalCode;
    private Long certificate;
    private String status;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_IdProfessor")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }
    @Column(name="user_IdUser")
    public Long getUser() {
        return user;
    }
    public void setUser(Long user){
        this.user = user;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="org_IdDepartment")
    public Long getDepartment() {
        return department;
    }
    public void setIdDepartment(Long department){
        this.department = department;
    }
    @Column(name="professionalCode")
    public String getProfessionalCode() {
        return professionalCode;
    }
    public void setProfessionalCode(String code){
        this.professionalCode = code;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    @Column(name="certificate")
    public Long getCertificate() {
        return certificate;
    }
    public void setCertificate(Long certificate){
        this.certificate = certificate;
    
    }
}
