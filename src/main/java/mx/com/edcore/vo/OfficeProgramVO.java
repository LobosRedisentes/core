/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProgOffices")
public class OfficeProgramVO implements Serializable {
    
    private Long id;
    private Long office;
    private Long program;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="prog_IdOffice")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="org_IdOffice")
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }

    @Column(name="prog_IdProgram")
    public Long getProgram() {
        return program;
    }
    public void setProgram(Long program) {
        this.program = program;
    }
}
