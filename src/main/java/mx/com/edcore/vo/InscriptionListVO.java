/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GroupInscriptionLists")
public class InscriptionListVO implements Serializable {
   
    private Long id;
    private Long groupInscription;
    private Long student;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdInscriptionList")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="group_IdGroupInscription")
    public Long getGroupInscription() {
        return groupInscription;
    }
    public void setGroupInscription(Long groupInscription) {
        this.groupInscription = groupInscription;
    }

    @Column(name="user_IdStudent")
    public Long getStudent() {
        return student;
    }
    public void setStudent(Long student) {
        this.student = student;
    }
}
