/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserStudents")
public class StudentVO implements Serializable {
    
    private Long id;
    private Long user;
    private Long plan;
    private Long specialty;
    private String code;
    private Long semester;
    private Long calendarStart;
    private Long calendarEnd;
    private String enroll;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_IdStudent")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="safe_IdUser")
    public Long getUser() {
        return user;
    }
    public void setUser(Long user) {
        this.user = user;
    }

    @Column(name="prog_IdPlan")
    public Long getPlan() {
        return plan;
    }
    public void setPlan(Long plan) {
        this.plan = plan;
    }

    @Column(name="prog_IdSpecialty")
    public Long getSpecialty() {
        return specialty;
    }
    public void setSpecialty(Long specialty) {
        this.specialty = specialty;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="semester")
    public Long getSemester() {
        return semester;
    }
    public void setSemester(Long semester) {
        this.semester = semester;
    }

    @Column(name="cal_IdCalSchoolStart")
    public Long getCalendarStart() {
        return calendarStart;
    }
    public void setCalendarStart(Long calendarStart) {
        this.calendarStart = calendarStart;
    }

    @Column(name="cal_IdCalSchoolEnd")
    public Long getCalendarEnd() {
        return calendarEnd;
    }
    public void setCalendarEnd(Long calendarEnd) {
        this.calendarEnd = calendarEnd;
    }

    @Column(name="enroll")
    public String getEnroll() {
        return enroll;
    }
    public void setEnroll(String enroll) {
        this.enroll = enroll;
    }

    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
