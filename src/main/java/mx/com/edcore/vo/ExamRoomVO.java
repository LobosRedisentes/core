/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsExamRooms")
public class ExamRoomVO implements Serializable {
    
    private Long id;
    private Long calendar;
    private Long room;
    private Long exam;
    private String description;
    private Long limit;
    private Date date;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdExamRoom")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="cal_IdCalSchool")
    public Long getCalendar() {
        return calendar;
    }
    public void setCalendar(Long calendar) {
        this.calendar = calendar;
    }
    
    @Column(name="org_IdRoom")
    public Long getRoom() {
        return room;
    }
    public void setRoom(Long room) {
        this.room = room;
    }

    @Column(name="ins_IdExam")
    public Long getExam() {
        return exam;
    }
    public void setExam(Long exam) {
        this.exam = exam;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="limit")
    public Long getLimit() {
        return limit;
    }
    public void setLimit(Long limit) {
        this.limit = limit;
    }

    @Column(name="date")
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
}
