/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*   Hugo Alejandro Armenta Briseño
*   04/05/2015
*   GroupList - POJO
**/
@Entity
@Table(name="GroupList")
public class GroupListVO implements java.io.Serializable{
    
    private Long id;
    private Long groupSubject;
    private Long student;
    
    public GroupListVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdList")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }
    @Column(name="group_IdGroupSubject")
    public Long getGroupSubject() {
        return this.groupSubject;
    }
    public void setGroupSubject(Long groupSubject){
        this.groupSubject = groupSubject;
    }
    @Column(name="user_IdStudent")
    public Long getStudent() {
        return this.student;
    }
    public void setUser_IdStudent(Long student){
        this.student = student;
    }
}
