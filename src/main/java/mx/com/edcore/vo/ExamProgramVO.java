/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsExamsPrograms")
public class ExamProgramVO implements Serializable {
    
    private Long id;
    private Long program;
    private Long exam;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdExamProgram")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="prog_IdProgram")
    public Long getProgram() {
        return program;
    }
    public void setProgram(Long program) {
        this.program = program;
    }

    @Column(name="ins_IdExam")
    public Long getExam() {
        return exam;
    }
    public void setExam(Long exam) {
        this.exam = exam;
    }
}
