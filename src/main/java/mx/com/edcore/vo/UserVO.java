/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SafeUsers")
public class UserVO implements Serializable {
    
    private Long id;
    private Long office;
    private String name;
    private String firstName;
    private String secondName;
    private Date birthDate;
    private Long birthTown;
    private String socialSecurity;
    private String blobFactor;
    private String sex;
    private String rfc;
    private String curp;
    private String occupation;
    private Long adressBook;
    private String status;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="safe_IdUser")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="org_IdOffice")
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Column(name="firstLast")
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @Column(name="secondLast")
    public String getSecondName() {
        return secondName;
    }
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    @Column(name="birthDate")
    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    @Column(name="birthTown")
    public Long getBirthTown() {
        return birthTown;
    }
    public void setBirthTown(Long birthTown) {
        this.birthTown = birthTown;
    }
    @Column(name="socialSecurity")
    public String getSocialSecurity() {
        return socialSecurity;
    }
    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }
    @Column(name="blobFactor")
    public String getBlobFactor() {
        return blobFactor;
    }
    public void setBlobFactor(String blobFactor) {
        this.blobFactor = blobFactor;
    }
    @Column(name="sex")
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    @Column(name="rfc")
    public String getRfc() {
        return rfc;
    }
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    @Column(name="curp")
    public String getCurp() {
        return curp;
    }
    public void setCurp(String curp) {
        this.curp = curp;
    }
    @Column(name="occupation")
    public String getOccupation() {
        return occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    @Column(name="addr_IdAddrBook")
    public Long getAdressBook() {
        return adressBook;
    }
    public void setAdressBook(Long adressBook) {
        this.adressBook = adressBook;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }    
}
