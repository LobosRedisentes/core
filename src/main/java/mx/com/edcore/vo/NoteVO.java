/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PubNotes")
public class NoteVO implements Serializable {
    
    private Long id;
    private Long office;
    private String code;
    private String body;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="pub_IdNote")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="org_IdOffice")
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="body")
    public String getBody() {
        return body;
    }
    public void setBody(String body) {
        this.body = body;
    }
}
