/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PubReleases")
public class ReleaseVO implements Serializable {
    
    private Long id;
    private Long office;
    private String title;
    private String description;
    private Blob image;
    private String link;
    private Integer position;
    private String panel;
    private String app;
    private String platform;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="pub_IdRelease")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="org_IdOffice")
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }
    @Column(name="title")
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name="description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name="image")
    public Blob getImage() {
        return image;
    }
    public void setImage(Blob image) {
        this.image = image;
    }

    @Column(name="link")
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }

    @Column(name="position")
    public Integer getPosition() {
        return position;
    }
    public void setPosition(Integer position) {
        this.position = position;
    }

    @Column(name="panel")
    public String getPanel() {
        return panel;
    }
    public void setPanel(String panel) {
        this.panel = panel;
    }
    @Column(name="app")
    public String getApp() {
        return app;
    }
    public void setApp(String app) {
        this.app = app;
    }
    @Column(name="platform")
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
