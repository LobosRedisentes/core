/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProgSpecialtyPlan")
public class SpecialtyPlanVO implements Serializable {
    
    private Long id;
    private Long specialty;
    private Long plan;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="prog_IdSpecialtyPlan")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="prog_IdSpecialty")
    public Long getSpecialty() {
        return specialty;
    }
    public void setSpecialty(Long specialty) {
        this.specialty = specialty;
    }

    @Column(name="prog_IdPlan")
    public Long getPlan() {
        return plan;
    }
    public void setPlan(Long plan) {
        this.plan = plan;
    }
}
