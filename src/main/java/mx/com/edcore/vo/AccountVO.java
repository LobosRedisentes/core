/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SafeAccounts")
public class AccountVO implements Serializable {
    
    private Long id;
    private Long user;
    private String code;
    private String email;
    private String password;
    private String recoverKey;
    private String status;
    private Long rol;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="safe_IdAccount")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="safe_IdUser")
    public Long getUser() {
        return user;
    }
    public void setUser(Long user) {
        this.user = user;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Column(name="email")
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    @Column(name="password")
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    @Column(name="recoverKey")
    public String getRecoverKey() {
        return recoverKey;
    }
    public void setRecoverKey(String recoverKey) {
        this.recoverKey = recoverKey;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    @Column(name="safe_IdRol")
    public Long getRol() {
        return rol;
    }
    public void setRol(Long rol) {
        this.rol = rol;
    }
}