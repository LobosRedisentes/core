/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PubUserGroups")
public class MessageUserGroupVO implements Serializable {

    private Long id;
    private Long group;
    private Long user;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pub_IdUserGroup")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "pub_IdGroup")
    public Long getGroup() {
        return group;
    }
    public void setGroup(Long group) {
        this.group = group;
    }

    @Column(name = "safe_IdUser")
    public Long getUser() {
        return user;
    }
    public void setAccount(Long user) {
        this.user = user;
    }
}
