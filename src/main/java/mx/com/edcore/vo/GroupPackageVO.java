/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Groups")
public class GroupPackageVO implements Serializable {

    private Long id;
    private Long plan;
    private Long calendar;
    private Long turn;
    private Long semester;
    private String code;
    private String name;
    private Long limitMax;
    private String type;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdGroup")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="prog_IdPlan")
    public Long getPlan() {
        return plan;
    }
    public void setPlan(Long plan) {
        this.plan = plan;
    }

    @Column(name="cal_IdCalSchool")
    public Long getCalendar() {
        return calendar;
    }
    public void setCalendar(Long calendar) {
        this.calendar = calendar;
    }

    @Column(name="sch_IdTurn")
    public Long getTurn() {
        return turn;
    }
    public void setTurn(Long turn) {
        this.turn = turn;
    }

    @Column(name="semester")
    public Long getSemester() {
        return semester;
    }
    public void setSemester(Long semester) {
        this.semester = semester;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name="limitMax")
    public Long getLimitMax() {
        return limitMax;
    }
    public void setLimitMax(Long limitMax) {
        this.limitMax = limitMax;
    }

    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
