/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserAdministrative")
public class AdministrativeVO implements Serializable {
    
    private Long id;
    private Long user;
    private Long department;
    private Long certificate;
    private Long workstation;
    private String code;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_IdAdministrative")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="safe_IdUser")
    public Long getUser() {
        return user;
    }
    public void setUser(Long user) {
        this.user = user;
    }

    @Column(name="org_IdDepartment")
    public Long getDepartment() {
        return department;
    }
    public void setDepartment(Long department) {
        this.department = department;
    }

    @Column(name="conf_IdCertificate")
    public Long getCertificate() {
        return certificate;
    }
    public void setCertificate(Long certificate) {
        this.certificate = certificate;
    }
    
    public Long getWorkstation() {
        return workstation;
    }
    public void setWorkstation(Long workstation) {
        this.workstation = workstation;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
