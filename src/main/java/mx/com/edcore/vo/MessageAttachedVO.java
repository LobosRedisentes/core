/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PubMsnAttached")
public class MessageAttachedVO implements Serializable {
    
    private Long id;
    private Long message;
    private Blob file;
    private String type;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="pub_IdAttached")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="pub_IdMessage")
    public Long getMessage() {
        return message;
    }
    public void setMessage(Long message) {
        this.message = message;
    }

    @Column(name="file")
    public Blob getFile() {
        return file;
    }
    public void setFile(Blob file) {
        this.file = file;
    }

    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
