/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsApplications")
public class ApplicationVO implements Serializable {
    
    private Long id;
    private Long applicant;
    private Long examRoom;
    private float average;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdApplication")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="user_IdApplicant")
    public Long getApplicant() {
        return applicant;
    }
    public void setApplicant(Long applicant) {
        this.applicant = applicant;
    }
    
    @Column(name="ins_IdExamRoom")
    public Long getExamRoom() {
        return examRoom;
    }
    public void setExamRoom(Long examRoom) {
        this.examRoom = examRoom;
    }
    
    @Column(name="average")
    public float getAverage() {
        return average;
    }
    public void setAverage(float average) {
        this.average = average;
    } 
}
