/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*   Hugo Alejandro Armenta Briseño
*   04/05/2015
*   ScheduleHours - POJO
**/
@Entity
@Table(name="ScheduleHours")
public class HourVO implements java.io.Serializable{
    
    private Long id;
    private Long turn;
    private Time start;
    private Time end;
    private String type;
    
    public HourVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="sch_IdHour")
    public Long getId() {
        return id;
    }
    public void setSch_IdHour(Long id){
        this.id = id;
    }
    @Column(name="sch_IdTurn")
    public Long getTurn() {
        return turn;
    }
    public void setTurn(Long turn){
        this.turn = turn;
    }
    @Column(name="start")
    public Time getStart() {
        return start;
    }
    public void setStart(Time start){
        this.start = start;
    }
    @Column(name="end")
    public Time getEnd() {
        return end;
    }
    public void setEnd(Time end){
        this.end = end;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
}
