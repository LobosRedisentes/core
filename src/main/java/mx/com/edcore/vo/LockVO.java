/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SafeLocks")
public class LockVO implements Serializable {
    
    private Long id;
    private String code;
    private String description;
    private Long fromUser;
    private Long toUser;
    private Date created;
    private Date removed;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="safe_IdLock")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Column(name="description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    @Column(name="fromUser")
    public Long getFromUser() {
        return fromUser;
    }
    public void setFromUser(Long fromUser) {
        this.fromUser = fromUser;
    }
    @Column(name="toUser")
    public Long getToUser() {
        return toUser;
    }
    public void setToUser(Long toUser) {
        this.toUser = toUser;
    }
    @Column(name="created")
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    @Column(name="removed")
    public Date getRemoved() {
        return removed;
    }
    public void setRemoved(Date removed) {
        this.removed = removed;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}