/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CalSchool")
public class CalendarVO implements Serializable {
    
    private Long id;
    private String code;
    private String abbreviation;
    private String name;
    private Date start;
    private Date startVacation;
    private Date endVacation;
    private Date end;
    private String type;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="cal_IdCalSchool")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Column(name="abreviation")
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Column(name="start")
    public Date getStart() {
        return start;
    }
    public void setStart(Date start) {
        this.start = start;
    }
    @Column(name="startVacation")
    public Date getStartVacation() {
        return startVacation;
    }
    public void setStartVacation(Date startVacation) {
        this.startVacation = startVacation;
    }
    @Column(name="endVacation")
    public Date getEndVacation() {
        return endVacation;
    }
    public void setEndVacation(Date endVacation) {
        this.endVacation = endVacation;
    }
    @Column(name="end")
    public Date getEnd() {
        return end;
    }
    public void setEnd(Date end) {
        this.end = end;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}