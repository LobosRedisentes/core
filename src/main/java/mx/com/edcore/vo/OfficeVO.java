/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OrgOffices")
public class OfficeVO implements Serializable  {
    
    private Long id;
    private String code;
    private String revoe;
    private String dgp;
    private String abbreviation;
    private String name;
    private String slogan;
    private String url;
    private Long address;
    private String key;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="org_IdOffice")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="revoe")
    public String getRevoe() {
        return revoe;
    }
    public void setRevoe(String revoe) {
        this.revoe = revoe;
    }

    @Column(name="dgp")
    public String getDgp() {
        return dgp;
    }
    public void setDgp(String dgp) {
        this.dgp = dgp;
    }
    
    @Column(name="abbreviation")
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name="slogan")
    public String getSlogan() {
        return slogan;
    }
    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Column(name="url")
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name="addr_IdAddrBook")
    public Long getAddress() {
        return address;
    }
    public void setAddress(Long address) {
        this.address = address;
    }

    @Column(name="key")
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    
}
