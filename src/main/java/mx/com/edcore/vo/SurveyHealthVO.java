/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="InsSurvey")
public class SurveyHealthVO implements Serializable {
    
    private Long id;
    private Long user;
    private String resp1;
    private String resp2;
    private String resp3;
    private String resp4;
    private String resp5;
    private String resp6;
    private String resp7;
    private String resp8;
    private String resp9;
    private String resp10;
    private String resp11;
    private String resp12;
    private String resp13;
    private String resp14;
    private String resp15;
    private String resp16;
    private String resp17;
    private String resp18;
    private String resp19;
    private String resp20;
    private String resp21;
    private String resp22;
    private String resp23;
    private String resp24;
    private String resp25;
    private String resp26;
    private String resp27;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdSurvey")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="safe_IdUser")
    public Long getUser() {
        return user;
    }
    public void setUser(Long user) {
        this.user = user;
    }

    @Column(name="resp1")
    public String getResp1() {
        return resp1;
    }
    public void setResp1(String resp1) {
        this.resp1 = resp1;
    }

    @Column(name="resp2")
    public String getResp2() {
        return resp2;
    }
    public void setResp2(String resp2) {
        this.resp2 = resp2;
    }

    @Column(name="resp3")
    public String getResp3() {
        return resp3;
    }
    public void setResp3(String resp3) {
        this.resp3 = resp3;
    }

    @Column(name="resp4")
    public String getResp4() {
        return resp4;
    }
    public void setResp4(String resp4) {
        this.resp4 = resp4;
    }

    @Column(name="resp5")
    public String getResp5() {
        return resp5;
    }
    public void setResp5(String resp5) {
        this.resp5 = resp5;
    }

    @Column(name="resp6")
    public String getResp6() {
        return resp6;
    }
    public void setResp6(String resp6) {
        this.resp6 = resp6;
    }

    @Column(name="resp7")
    public String getResp7() {
        return resp7;
    }
    public void setResp7(String resp7) {
        this.resp7 = resp7;
    }

    @Column(name="resp8")
    public String getResp8() {
        return resp8;
    }
    public void setResp8(String resp8) {
        this.resp8 = resp8;
    }

    @Column(name="resp9")
    public String getResp9() {
        return resp9;
    }
    public void setResp9(String resp9) {
        this.resp9 = resp9;
    }

    @Column(name="resp10")
    public String getResp10() {
        return resp10;
    }
    public void setResp10(String resp10) {
        this.resp10 = resp10;
    }

    @Column(name="resp11")
    public String getResp11() {
        return resp11;
    }
    public void setResp11(String resp11) {
        this.resp11 = resp11;
    }

    @Column(name="resp12")
    public String getResp12() {
        return resp12;
    }
    public void setResp12(String resp12) {
        this.resp12 = resp12;
    }

    @Column(name="resp13")
    public String getResp13() {
        return resp13;
    }
    public void setResp13(String resp13) {
        this.resp13 = resp13;
    }

    @Column(name="resp14")
    public String getResp14() {
        return resp14;
    }
    public void setResp14(String resp14) {
        this.resp14 = resp14;
    }

    @Column(name="resp15")
    public String getResp15() {
        return resp15;
    }
    public void setResp15(String resp15) {
        this.resp15 = resp15;
    }

    @Column(name="resp16")
    public String getResp16() {
        return resp16;
    }
    public void setResp16(String resp16) {
        this.resp16 = resp16;
    }

    @Column(name="resp17")
    public String getResp17() {
        return resp17;
    }
    public void setResp17(String resp17) {
        this.resp17 = resp17;
    }

    @Column(name="resp18")
    public String getResp18() {
        return resp18;
    }
    public void setResp18(String resp18) {
        this.resp18 = resp18;
    }

    @Column(name="resp19")
    public String getResp19() {
        return resp19;
    }
    public void setResp19(String resp19) {
        this.resp19 = resp19;
    }

    @Column(name="resp20")
    public String getResp20() {
        return resp20;
    }
    public void setResp20(String resp20) {
        this.resp20 = resp20;
    }

    @Column(name="resp21")
    public String getResp21() {
        return resp21;
    }
    public void setResp21(String resp21) {
        this.resp21 = resp21;
    }

    @Column(name="resp22")
    public String getResp22() {
        return resp22;
    }
    public void setResp22(String resp22) {
        this.resp22 = resp22;
    }

    @Column(name="resp23")
    public String getResp23() {
        return resp23;
    }
    public void setResp23(String resp23) {
        this.resp23 = resp23;
    }

    @Column(name="resp24")
    public String getResp24() {
        return resp24;
    }
    public void setResp24(String resp24) {
        this.resp24 = resp24;
    }

    @Column(name="resp25")
    public String getResp25() {
        return resp25;
    }
    public void setResp25(String resp25) {
        this.resp25 = resp25;
    }

    @Column(name="resp26")
    public String getResp26() {
        return resp26;
    }
    public void setResp26(String resp26) {
        this.resp26 = resp26;
    }

    @Column(name="resp27")
    public String getResp27() {
        return resp27;
    }
    public void setResp27(String resp27) {
        this.resp27 = resp27;
    }
}
