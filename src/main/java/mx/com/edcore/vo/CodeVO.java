/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsCodes")
public class CodeVO implements Serializable {
    
    private Long id;
    private String code;
    private Long calendar;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdCode")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="cal_IdCalSchool")
    public Long getCalendar() {
        return calendar;
    }
    public void setCalendar(Long calendar) {
        this.calendar = calendar;
    }
}
