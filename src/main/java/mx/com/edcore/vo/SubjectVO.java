package mx.com.edcore.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Subjects per program table
 * @author Ricardo Romo Ramírez
 * @version 1.0.0
 */
@Entity
@Table(name="ProgSubjects")
public class SubjectVO implements java.io.Serializable{
    
    private Long id;
    private Long plan;
    private Long department;
    private String code;
    private String officialCode;
    private String abbreviation;
    private String name;
    private Long credits;
    private Long reqCredits;
    private Long reqSemester;
    private Long hrsPractices;
    private Long hrsTheorist;
    private Long semester;
    private Long row;
    private Long column;
    private String requiered;
    private String evaluates;
    private String averaged;
    private String type;
    
    public SubjectVO(){}
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="prog_IdSubject")
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    @Column(name="prog_IdPlan")
    public Long getPlan() {
        return plan;
    }
    public void setPlan(Long plan){
        this.plan = plan;
    }
    
    @Column(name="org_IdDepartment")
    public Long getDepartment() {
        return department;
    }
    public void setDepartment(Long department){
        this.department = department;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="officialCode")
    public String getOfficialCode() {
        return officialCode;
    }
    public void setOfficialCode(String officialCode){
        this.officialCode = officialCode;
    }
    
    @Column(name="abbreviation")
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation){
        this.abbreviation = abbreviation;
    } 
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="credits")
    public Long getCredits() {
        return credits;
    }
    public void setCredits(Long credits){
        this.credits = credits;
    }
    @Column(name="reqCredits")
    public Long getReqCredits() {
        return reqCredits;
    }
    public void setReqCredits(Long reqCredits){
        this.reqCredits = reqCredits;
    }
    
    @Column(name="reqSemester")
    public Long getReqSemester() {
        return reqSemester;
    }
    public void setReqSemester(Long reqSemester){
        this.reqSemester = reqSemester;
    }
    
    @Column(name="hrsPractices")
    public Long getHrsPractices() {
        return hrsPractices;
    }
    public void setHrsPractices(Long hrsPractices){
        this.hrsPractices = hrsPractices;
    }
    
    @Column(name="hrsTheorist")
    public Long getHrsTheorist() {
        return hrsTheorist;
    }
    public void setHrsTheorist(Long hrsTheorist){
        this.hrsTheorist = hrsTheorist;
    }
    
    @Column(name="semester")
    public Long getSemester() {
        return semester;
    }
    public void setSemester(Long semester){
        this.semester = semester;
    }
    
    @Column(name="row")
    public Long getRow() {
        return row;
    }
    public void setRow(Long row){
        this.row = row;
    }
    
    @Column(name="column")
    public Long getColumn() {
        return column;
    }
    public void setColumn(Long column){
        this.column = column;
    }
    
    @Column(name="requiered")
    public String getRequiered() {
        return requiered;
    }
    public void setRequiered(String requiered){
        this.requiered = requiered;
    }
    
    @Column(name="evaluates")
    public String getEvaluates() {
        return evaluates;
    }
    public void setEvaluates(String evaluates){
        this.evaluates = evaluates;
    }
    
    @Column(name="averaged")
    public String getAveraged() {
        return averaged;
    }
    public void setAveraged(String averaged){
        this.averaged = averaged;
    }
    
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
}
