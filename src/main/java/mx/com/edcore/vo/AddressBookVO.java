/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AddrBook")
public class AddressBookVO implements Serializable {
    
    private Long id;
    private Long location;
    private String street;
    private String numExternal;
    private String numInternal;
    private String phone;
    private String cellPhone;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="addr_IdAddrBook")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="addr_IdLocation")
    public Long getLocation() {
        return location;
    }
    public void setLocation(Long location) {
        this.location = location;
    }
    @Column(name="street")
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    @Column(name="numExternal")
    public String getNumExternal() {
        return numExternal;
    }
    public void setNumExternal(String numExternal) {
        this.numExternal = numExternal;
    }
    @Column(name="numInternal")
    public String getNumInternal() {
        return numInternal;
    }
    public void setNumInternal(String numInternal) {
        this.numInternal = numInternal;
    }
    @Column(name="phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    @Column(name="cellPhone")
    public String getCellPhone() {
        return cellPhone;
    }
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }
}