/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GroupSubject")
public class GroupShared implements Serializable {

    private Long id;
    private Long base;
    private Long reference;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdSubjectShared")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="group_IdBaseSubject")
    public Long getBase() {
        return base;
    }
    public void setBase(Long base) {
        this.base = base;
    }

    @Column(name="group_IdRefSubject")
    public Long getReference() {
        return reference;
    }
    public void setReference(Long reference) {
        this.reference = reference;
    }
}
