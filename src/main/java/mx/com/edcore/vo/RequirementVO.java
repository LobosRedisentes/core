/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProgRequirements")
public class RequirementVO implements Serializable {
    
    private Long id;
    private Long from;
    private Long to;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="prog_IdRequirement")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="from")
    public Long getFrom() {
        return from;
    }
    public void setFrom(Long from) {
        this.from = from;
    }

    @Column(name="to")
    public Long getTo() {
        return to;
    }
    public void setTo(Long to) {
        this.to = to;
    }
}
