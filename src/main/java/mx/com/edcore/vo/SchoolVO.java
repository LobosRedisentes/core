/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Schools")
public class SchoolVO implements Serializable {
    
    private Long id;
    private Long schoolSubLevel;
    private Long state;
    private String code;
    private String name;
    private String type;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="schl_IdSchool")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="schl_IdSubLevel")
    public Long getSchoolSubLevel() {
        return schoolSubLevel;
    }
    public void setSchoolSubLevel(Long schoolSubLevel) {
        this.schoolSubLevel = schoolSubLevel;
    }
    @Column(name="addr_IdState")
    public Long getState() {
        return state;
    }
    public void setState(Long state) {
        this.state = state;
    }
    @Column(name="code")
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Column(name="type")
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
