/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "UserTutor")
public class TutorVO implements Serializable {

    private Long id;
    private Long applicant;
    private String name;
    private Long location;
    private String street;
    private String numExternal;
    private String numInternal;
    private String phone;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="user_IdTutor")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="user_IdApplicant")
    public Long getApplicant() {
        return applicant;
    }
    public void setApplicant(Long applicant) {
        this.applicant = applicant;
    }
    @Column(name="name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Column(name="addr_IdLocation")
    public Long getLocation() {
        return location;
    }
    public void setLocation(Long location) {
        this.location = location;
    }
    @Column(name="street")
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    @Column(name="numExternal")
    public String getNumExternal() {
        return numExternal;
    }
    public void setNumExternal(String numExternal) {
        this.numExternal = numExternal;
    }
    @Column(name="numInternal")
    public String getNumInternal() {
        return numInternal;
    }
    public void setNumInternal(String numInternal) {
        this.numInternal = numInternal;
    }
    @Column(name="phone")
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
