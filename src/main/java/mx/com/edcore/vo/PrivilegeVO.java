/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SafePrivileges")
public class PrivilegeVO implements Serializable {
    
    private Long id;
    private Long operation;
    private Long rol;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="safe_IdPrivilege")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="safe_IdOperation")
    public Long getOperation() {
        return operation;
    }
    public void setOperation(Long operation) {
        this.operation = operation;
    }
    @Column(name="safe_IdRol")
    public Long getRol() {
        return rol;
    }
    public void setRol(Long rol) {
        this.rol = rol;
    }
}