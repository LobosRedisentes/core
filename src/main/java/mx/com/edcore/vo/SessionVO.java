/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SafeSessions")
public class SessionVO implements Serializable {
    
    private Long id;
    private Long account;
    private Date opened;
    private Date closed;
    private String ip;
    private String panel;
    private String app;
    private String platform;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="safe_IdSession")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="safe_IdAccount")
    public Long getAccount() {
        return account;
    }
    public void setAccount(Long account) {
        this.account = account;
    }
    @Column(name="opened")
    public Date getOpened() {
        return opened;
    }
    public void setOpened(Date opened) {
        this.opened = opened;
    }
    @Column(name="closed")
    public Date getClosed() {
        return closed;
    }
    public void setClosed(Date closed) {
        this.closed = closed;
    }
    @Column(name="ip")
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    @Column(name="panel")
    public String getPanel() {
        return panel;
    }
    public void setPanel(String panel) {
        this.panel = panel;
    }
    @Column(name="app")
    public String getApp() {
        return app;
    }
    public void setApp(String app) {
        this.app = app;
    }
    @Column(name="platform")
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
