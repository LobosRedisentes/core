/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsListAccepted")
public class AcceptedListVO implements Serializable {
    
    private Long id;
    private Long applicant;
    private Long calendar;
    private Long plan;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdListAccepted")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="user_IdApplicant")
    public Long getApplicant() {
        return applicant;
    }
    public void setApplicant(Long applicant) {
        this.applicant = applicant;
    }

    @Column(name="cal_IdCalSchool")
    public Long getCalendar() {
        return calendar;
    }
    public void setCalendar(Long calendar) {
        this.calendar = calendar;
    }

    @Column(name="prog_IdPlan")
    public Long getPlan() {
        return plan;
    }
    public void setPlan(Long plan) {
        this.plan = plan;
    }
}
