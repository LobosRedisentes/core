/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "InsSurvey")
public class ApplicantSurveyVO implements Serializable {
   
    private Long id;
    private Long applicant;
    private Long resp1;
    private Long resp2;
    private Long resp3;
    private Long resp4;
    private Long resp5;
    private Long resp6;
    private Long resp7;
    private Long resp8;
    private Long resp9;
    private Long resp10;
    private Long resp11;
    private Long resp12;
    private Long resp13;
    private Long resp14;
    private Long resp15;
    private Long resp16;
    private Long resp17;
    private Long resp18;
    private Long resp19;
    private Long resp20;
    private Long resp21;
    private Long resp22;
    private Long resp23;
    private Long resp24;
    private Long resp25;
    private Long resp26;
    private Long resp27;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ins_IdSurvey")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="user_IdApplicant")
    public Long getApplicant() {
        return applicant;
    }
    public void setApplicant(Long applicant) {
        this.applicant = applicant;
    }

    @Column(name="resp1")
    public Long getResp1() {
        return resp1;
    }
    public void setResp1(Long resp1) {
        this.resp1 = resp1;
    }

    @Column(name="resp2")
    public Long getResp2() {
        return resp2;
    }
    public void setResp2(Long resp2) {
        this.resp2 = resp2;
    }

    @Column(name="resp3")
    public Long getResp3() {
        return resp3;
    }
    public void setResp3(Long resp3) {
        this.resp3 = resp3;
    }

    @Column(name="resp4")
    public Long getResp4() {
        return resp4;
    }
    public void setResp4(Long resp4) {
        this.resp4 = resp4;
    }

    @Column(name="resp5")
    public Long getResp5() {
        return resp5;
    }
    public void setResp5(Long resp5) {
        this.resp5 = resp5;
    }

    @Column(name="resp6")
    public Long getResp6() {
        return resp6;
    }
    public void setResp6(Long resp6) {
        this.resp6 = resp6;
    }
    
    @Column(name="resp7")
    public Long getResp7() {
        return resp7;
    }
    public void setResp7(Long resp7) {
        this.resp7 = resp7;
    }
    
    @Column(name="resp8")
    public Long getResp8() {
        return resp8;
    }
    public void setResp8(Long resp8) {
        this.resp8 = resp8;
    }

    @Column(name="resp9")
    public Long getResp9() {
        return resp9;
    }
    public void setResp9(Long resp9) {
        this.resp9 = resp9;
    }

    @Column(name="resp10")
    public Long getResp10() {
        return resp10;
    }
    public void setResp10(Long resp10) {
        this.resp10 = resp10;
    }

    @Column(name="resp11")
    public Long getResp11() {
        return resp11;
    }
    public void setResp11(Long resp11) {
        this.resp11 = resp11;
    }

    @Column(name="resp12")
    public Long getResp12() {
        return resp12;
    }
    public void setResp12(Long resp12) {
        this.resp12 = resp12;
    }

    @Column(name="resp13")
    public Long getResp13() {
        return resp13;
    }
    public void setResp13(Long resp13) {
        this.resp13 = resp13;
    }

    @Column(name="resp14")
    public Long getResp14() {
        return resp14;
    }
    public void setResp14(Long resp14) {
        this.resp14 = resp14;
    }

    @Column(name="resp15")
    public Long getResp15() {
        return resp15;
    }
    public void setResp15(Long resp15) {
        this.resp15 = resp15;
    }

    @Column(name="resp16")
    public Long getResp16() {
        return resp16;
    }
    public void setResp16(Long resp16) {
        this.resp16 = resp16;
    }

    @Column(name="resp17")
    public Long getResp17() {
        return resp17;
    }
    public void setResp17(Long resp17) {
        this.resp17 = resp17;
    }

    @Column(name="resp18")
    public Long getResp18() {
        return resp18;
    }
    public void setResp18(Long resp18) {
        this.resp18 = resp18;
    }
    
    @Column(name="resp19")
    public Long getResp19() {
        return resp19;
    }
    public void setResp19(Long resp19) {
        this.resp19 = resp19;
    }

    @Column(name="resp20")
    public Long getResp20() {
        return resp20;
    }
    public void setResp20(Long resp20) {
        this.resp20 = resp20;
    }

    @Column(name="resp21")
    public Long getResp21() {
        return resp21;
    }
    public void setResp21(Long resp21) {
        this.resp21 = resp21;
    }

    @Column(name="resp22")
    public Long getResp22() {
        return resp22;
    }
    public void setResp22(Long resp22) {
        this.resp22 = resp22;
    }

    @Column(name="resp23")
    public Long getResp23() {
        return resp23;
    }
    public void setResp23(Long resp23) {
        this.resp23 = resp23;
    }

    @Column(name="resp24")
    public Long getResp24() {
        return resp24;
    }
    public void setResp24(Long resp24) {
        this.resp24 = resp24;
    }

    @Column(name="resp25")
    public Long getResp25() {
        return resp25;
    }
    public void setResp25(Long resp25) {
        this.resp25 = resp25;
    }

    @Column(name="resp26")
    public Long getResp26() {
        return resp26;
    }
    public void setResp26(Long resp26) {
        this.resp26 = resp26;
    }

    @Column(name="resp27")
    public Long getResp27() {
        return resp27;
    }
    public void setResp27(Long resp27) {
        this.resp27 = resp27;
    }
}
