/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GroupSubject")
public class GroupVO implements Serializable {

    private Long id;
    private Long group;
    private Long subject;
    private Long professor;
    private Long code;
    private Long name;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdGroupSubject")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="group_IdGroup")
    public Long getGroup() {
        return group;
    }
    public void setGroup(Long group) {
        this.group = group;
    }

    @Column(name="prog_IdSubject")
    public Long getSubject() {
        return subject;
    }
    public void setSubject(Long subject) {
        this.subject = subject;
    }

    @Column(name="user_IdProfessor")
    public Long getProfessor() {
        return professor;
    }
    public void setProfessor(Long professor) {
        this.professor = professor;
    }

    @Column(name="code")
    public Long getCode() {
        return code;
    }
    public void setCode(Long code) {
        this.code = code;
    }

    @Column(name="name")
    public Long getName() {
        return name;
    }
    public void setName(Long name) {
        this.name = name;
    }
}
