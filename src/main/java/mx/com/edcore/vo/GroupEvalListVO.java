/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.vo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GroupListEval")
public class GroupEvalListVO implements Serializable {

    private Long id;
    private Long groupList;
    private Long evaluation;
    private float grade;
    private String option;
    private String status;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="group_IdListEval")
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="group_IdList")
    public Long getGroupList() {
        return groupList;
    }
    public void setGroupList(Long groupList) {
        this.groupList = groupList;
    }

    @Column(name="evaluation")
    public Long getEvaluation() {
        return evaluation;
    }
    public void setEvaluation(Long evaluation) {
        this.evaluation = evaluation;
    }

    @Column(name="grade")
    public float getGrade() {
        return grade;
    }
    public void setGrade(float grade) {
        this.grade = grade;
    }

    @Column(name="option")
    public String getOption() {
        return option;
    }
    public void setOption(String option) {
        this.option = option;
    }

    @Column(name="status")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
