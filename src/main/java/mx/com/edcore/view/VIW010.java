package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de promedios por semestre
 * @author Ricardo Romo Ramírez
 * @version 1.0.0
 */
@Entity
@Table(name="VIW010")
public class VIW010 implements java.io.Serializable{
    
    private int user_IdStudent;
    private String code;
    private String name;
    private String firstLast;
    private String secondLast;
    private int prog_IdProgram;
    private String program;
    private int prog_IdPlan;
    private String plan;
    private String status;
    private int cal_IdCalSchool;
    private String calendar;
    private float average;
    private int subjects;
    
    public VIW010(){}
    
    @Id
    @Column(name="user_IdStudent", insertable = false, updatable = false)
    public int getIdStudent() {
        return user_IdStudent;
    }
    public void setIdStudent(int user_IdStudent){
        this.user_IdStudent = user_IdStudent;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    
    @Column(name="prog_IdProgram", insertable = false, updatable = false)
    public int getIdProgram() {
        return prog_IdProgram;
    }
    public void setIdProgram(int prog_IdProgram){
        this.prog_IdProgram = prog_IdProgram;
    }
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    
    @Column(name="prog_IdPlan", insertable = false, updatable = false)
    public int getIdPlan() {
        return prog_IdPlan;
    }
    public void setIdPlan(int prog_IdPlan){
        this.prog_IdPlan = prog_IdPlan;
    }
    
    @Column(name="plan", insertable = false, updatable = false)
    public String getPlan() {
        return plan;
    }
    public void setPlan(String plan){
        this.plan = plan;
    }
    
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    
    @Column(name="cal_IdCalSchool", insertable = false, updatable = false)
    public int getIdCalSchool() {
        return cal_IdCalSchool;
    }
    public void setIdCalSchool(int cal_IdCalSchool){
        this.cal_IdCalSchool = cal_IdCalSchool;
    }
    
    @Column(name="calendar", insertable = false, updatable = false)
    public String getCalendar() {
        return calendar;
    }
    public void setCalendar(String calendar){
        this.calendar = calendar;
    }
    
    @Column(name="average", insertable = false, updatable = false)
    public float getAverage() {
        return average;
    }
    public void setAverage(float average){
        this.average = average;
    }
    
    @Column(name="subjects", insertable = false, updatable = false)
    public int getSubjects() {
        return subjects;
    }
    public void setSubjects(int subjects){
        this.subjects = subjects;
    }
}
