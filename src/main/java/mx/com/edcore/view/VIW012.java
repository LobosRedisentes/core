/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de aspirantes
 * @author Gabriel Cisneros Landeros
 * @version 1.0.0
 */
@Entity
@Table(name="VIW012")
public class VIW012 implements java.io.Serializable{
    
    private int dm_IdCertificate; 
    private int user_IdStudent;  
    private String code;
    private String name;
    private String firstLast;
    private String secondLast;  
    private int prog_IdProgram;
    private String program;
    private int prog_IdPlan;
    private String plan;
    private String book;
    private String sheet;
    private String date;
    private String type;
    private String status;
//    
    public VIW012(){}
    
   
    @Id
    @Column(name="dm_IdCertificate", insertable = false, updatable = false)
    public int getIdCertificate() {
        return dm_IdCertificate;
    }
    public void setIdCertificate(int dm_IdCertificate){
        this.dm_IdCertificate = dm_IdCertificate;
    }
     @Column(name="user_IdStudent", insertable = false, updatable = false)
    public int getIdStudent() {
        return user_IdStudent;
    }
    public void setIdStudent(int user_IdStudent){
        this.user_IdStudent = user_IdStudent;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    
   @Column(name="prog_IdProgram", insertable = false, updatable = false)
    public int getIdProgram() {
        return prog_IdProgram;
    }
    public void setIdProgram(int prog_IdProgram){
        this.prog_IdProgram = prog_IdProgram;
    }
    
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    
   @Column(name="prog_IdPlan", insertable = false, updatable = false)
    public int getIdPlan() {
        return prog_IdPlan;
    }
    public void setIdPlan(int prog_IdPlan){
        this.prog_IdPlan = prog_IdPlan;
    }
    @Column(name="plan", insertable = false, updatable = false)
    public String getplan() {
        return plan;
    }
    public void setplan(String plan){
        this.plan = plan;
    }
    @Column(name="book", insertable = false, updatable = false)
    public String getBook() {
        return book;
    }
    public void setBook(String book){
        this.book = book;
    }
    @Column(name="sheet", insertable = false, updatable = false)
    public String getSheet() {
        return sheet;
    }
    public void setSheet(String sheet){
        this.sheet = sheet;
    }
    @Column(name="date", insertable = false, updatable = false)
    public String getDate() {
        return date;
    }
    public void setDate(String date){
        this.date = date;
    }
    @Column(name="type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    
}
