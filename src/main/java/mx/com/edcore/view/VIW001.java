 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de estudiantes con 70%+ de creditos
 * @author Ricardo Romo Ramírez
 * @version 1.0.0
 */
@Entity
@Table(name="VIW001")
public class VIW001 implements java.io.Serializable{
    
    private int user_IdStudent;
    private String code;
    private String name;
    private String firstLast;
    private String secondLast;
    private int prog_IdPlan;
    private String plan;
    private int prog_IdProgram;
    private String program;
    private int porcCredApproved;
    private int credApproved;
    private int credTotal;
    private int credReqServ;
    private String status;
    
    public VIW001(){}
    
    @Id
    @Column(name="user_IdStudent", insertable = false, updatable = false)
    public int getIdStudent() {
        return user_IdStudent;
    }
    public void setIdStudent(int user_IdStudent){
        this.user_IdStudent = user_IdStudent;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    
    @Column(name="prog_IdPlan", insertable = false, updatable = false)
    public int getIdPlan() {
        return prog_IdPlan;
    }
    public void setIdPlan(int prog_IdPlan){
        this.prog_IdPlan = prog_IdPlan;
    }
    @Column(name="plan", insertable = false, updatable = false)
    public String getPlan() {
        return plan;
    }
    public void setPlan(String plan){
        this.plan = plan;
    }
    @Column(name="prog_IdProgram", insertable = false, updatable = false)
    public int getIdProgram() {
        return prog_IdProgram;
    }
    public void setIdProgram(int prog_IdProgram){
        this.prog_IdProgram = prog_IdProgram;
    }
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    @Column(name="porcCredApproved", insertable = false, updatable = false)
    public int getPorcCredApproved() {
        return porcCredApproved;
    }
    public void setPorcCredApproved(int porcCredApproved){
        this.porcCredApproved = porcCredApproved;
    }
    @Column(name="credApproved", insertable = false, updatable = false)
    public int getCredApproved() {
        return credApproved;
    }
    public void setCredApproved(int credApproved){
        this.credApproved = credApproved;
    }
    @Column(name="credTotal", insertable = false, updatable = false)
    public int getCredTotal() {
        return credTotal;
    }
    public void setCredTotal(int credTotal){
        this.credTotal = credTotal;
    }
    @Column(name="credReqServ", insertable = false, updatable = false)
    public int getCredReqServ() {
        return credReqServ;
    }
    public void setCredReqServ(int credReqServ){
        this.credReqServ = credReqServ;
    }
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
}
