/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIW058")
public class VIW058 implements Serializable {

    private Long id;
    private String name;
    private String firstLast;
    private String secondLast;
    private String sex;
    private String birthDate;
    private Long birthState;
    private String birthStateName;
    private Long birthTown;
    private String birthTownName;
    private String socialSecurity;
    private String blobFactor;
    private String rfc;
    private String curp;
    private String occupation;
    private String status;
    private Long address;
    private Long office;
    private String officeName;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast) {
        this.firstLast = firstLast;
    }

    @Column(name = "secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast) {
        this.secondLast = secondLast;
    }

    @Column(name = "sex", insertable = false, updatable = false)
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "birthDate", insertable = false, updatable = false)
    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Column(name = "birthState", insertable = false, updatable = false)
    public Long getBirthState() {
        return birthState;
    }
    public void setBirthState(Long birthState) {
        this.birthState = birthState;
    }

    @Column(name = "birthStateName", insertable = false, updatable = false)
    public String getBirthStateName() {
        return birthStateName;
    }
    public void setBirthStateName(String birthStateName) {
        this.birthStateName = birthStateName;
    }

    @Column(name = "birthTown", insertable = false, updatable = false)
    public Long getBirthTown() {
        return birthTown;
    }
    public void setBirthTown(Long birthTown) {
        this.birthTown = birthTown;
    }

    @Column(name = "birthTownName", insertable = false, updatable = false)
    public String getBirthTownName() {
        return birthTownName;
    }
    public void setBirthTownName(String birthTownName) {
        this.birthTownName = birthTownName;
    }

    @Column(name = "socialSecurity", insertable = false, updatable = false)
    public String getSocialSecurity() {
        return socialSecurity;
    }
    public void setSocialSecurity(String socialSecurity) {
        this.socialSecurity = socialSecurity;
    }

    @Column(name = "blobFactor", insertable = false, updatable = false)
    public String getBlobFactor() {
        return blobFactor;
    }
    public void setBlobFactor(String blobFactor) {
        this.blobFactor = blobFactor;
    }

    @Column(name = "rfc", insertable = false, updatable = false)
    public String getRfc() {
        return rfc;
    }
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @Column(name = "curp", insertable = false, updatable = false)
    public String getCurp() {
        return curp;
    }
    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Column(name = "occupation", insertable = false, updatable = false)
    public String getOccupation() {
        return occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Column(name = "status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "address", insertable = false, updatable = false)
    public Long getAddress() {
        return address;
    }
    public void setAddress(Long address) {
        this.address = address;
    }
    
    @Column(name = "office", insertable = false, updatable = false)
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }

    @Column(name = "officeName", insertable = false, updatable = false)
    public String getOfficeName() {
        return officeName;
    }
    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }
}
