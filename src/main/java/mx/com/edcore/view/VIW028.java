/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIW028")
public class VIW028 implements Serializable {

    private Long id;
    private Long office;
    private String officeName;
    private Long user;
    private String code;
    private String email;
    private String name;
    private String firstLast;
    private String secondLast;
    private String curp;
    private Long rol;
    private String rolName;
    private String password;
    private String recoverkey;
    private String type;
    private String status;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "office", insertable = false, updatable = false)
    public Long getOffice() {
        return office;
    }

    public void setOffice(Long office) {
        this.office = office;
    }
    
    @Column(name = "officeName", insertable = false, updatable = false)
    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }
    
    @Column(name = "user", insertable = false, updatable = false)
    public Long getUser() {
        return user;
    }
    public void setUser(Long user) {
        this.user = user;
    }

    @Column(name = "code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "email", insertable = false, updatable = false)
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }

    public void setFirstLast(String firstLast) {
        this.firstLast = firstLast;
    }
    
    @Column(name = "secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast) {
        this.secondLast = secondLast;
    }

    @Column(name = "curp", insertable = false, updatable = false)
    public String getCurp() {
        return curp;
    }
    public void setCurp(String curp) {
        this.curp = curp;
    }

    @Column(name = "rol", insertable = false, updatable = false)
    public Long getRol() {
        return rol;
    }

    public void setRol(Long rol) {
        this.rol = rol;
    }

    @Column(name = "rolName", insertable = false, updatable = false)
    public String getRolName() {
        return rolName;
    }
    public void setRolName(String rolName) {
        this.rolName = rolName;
    }

    @Column(name = "password", insertable = false, updatable = false)
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "recoverkey", insertable = false, updatable = false)
    public String getRecoverkey() {
        return recoverkey;
    }
    public void setRecoverkey(String recoverkey) {
        this.recoverkey = recoverkey;
    }

    @Column(name = "type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

}
