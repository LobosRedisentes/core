/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*   Hugo Alejandro Armenta Briseño
*   09/04/2015
*   VIW004 - Grupos Ocupación
**/
@Entity
@Table(name="VIW009")
public class VIW009 implements java.io.Serializable{
    
    //private int safe_IdUser;
    private int group_IdGroupSubject;
    private String group;
    private String subject;
    private String base;
    private int students;
    private String code;
    private String name;
    private String firstLast;
    private String secondLast;
    private String department;
    private String program;
    private String plan;
    private int cal_IdCalSchool;
    private String calendar;
    
    public VIW009(){}
    
    /*@Column(name="safe_IdUser", insertable = false, updatable = false)
    public int getIdUser() {
        return safe_IdUser;
    }
    public void setIdUser(int safe_IdUser){
        this.safe_IdUser = safe_IdUser;
    }*/
    @Id
    @Column(name="group_IdGroupSubject", insertable = false, updatable = false)
    public int getIdGroupSubject() {
        return group_IdGroupSubject;
    }
    public void setIdGroupSubject(int group_IdGroupSubject){
        this.group_IdGroupSubject = group_IdGroupSubject;
    }
    @Column(name="group", insertable = false, updatable = false)
    public String getGroup() {
        return group;
    }
    public void setGroup(String group){
        this.group = group;
    }
    @Column(name="subject", insertable = false, updatable = false)
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject){
        this.subject = subject;
    }
    @Column(name="base", insertable = false, updatable = false)
    public String getBase() {
        return base;
    }
    public void setBase(String base){
        this.base = base;
    }
    @Column(name="students", insertable = false, updatable = false)
    public int getStudents() {
        return students;
    }
    public void setStudents(int students){
        this.students = students;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    @Column(name="department", insertable = false, updatable = false)
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department){
        this.department = department;
    }
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    @Column(name="plan", insertable = false, updatable = false)
    public String getPlan() {
        return plan;
    }
    public void setPlan(String plan){
        this.plan = plan;
    }
    @Column(name="cal_IdCalSchool", insertable = false, updatable = false)
    public int getIdCalendar() {
        return cal_IdCalSchool;
    }
    public void setIdCalendar(int cal_IdCalSchool){
        this.cal_IdCalSchool = cal_IdCalSchool;
    }
    @Column(name="calendar", insertable = false, updatable = false)
    public String getCalendar() {
        return calendar;
    }
    public void setCalendar(String calendar){
        this.calendar = calendar;
    }
}
