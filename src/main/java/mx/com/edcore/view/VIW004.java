/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
*   Hugo Alejandro Armenta Briseño
*   05/03/2015
*   VIW004 - Alumnos convalidación
**/
@Entity
@Table(name="VIW004")
public class VIW004 implements java.io.Serializable{
    
    private int safe_IdUser;
    private int conv_IdResolution;
    private String code;
    private String firstLast;
    private String secondLast;
    private String plan;
    private String program;
    private int subjects;
    private String type;
    private String status;
    private int cal_IdCalSchool;
    private String calendar;
    
    public VIW004(){}
    
    @Column(name="safe_IdUser", insertable = false, updatable = false)
    public int getIdUser() {
        return safe_IdUser;
    }
    public void setIdUser(int safe_IdUser){
        this.safe_IdUser = safe_IdUser;
    }
    @Id
    @Column(name="conv_IdResolution", insertable = false, updatable = false)
    public int getIdResolution() {
        return conv_IdResolution;
    }
    public void setIdResolution(int conv_IdResolution){
        this.conv_IdResolution = conv_IdResolution;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    @Column(name="plan", insertable = false, updatable = false)
    public String getPlan() {
        return plan;
    }
    public void setPlan(String plan){
        this.plan = plan;
    }
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    @Column(name="subjects", insertable = false, updatable = false)
    public int getSubjects() {
        return subjects;
    }
    public void setSubjects(int subjects){
        this.subjects = subjects;
    }
    @Column(name="type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    @Column(name="cal_IdCalSchool", insertable = false, updatable = false)
    public int getIdCalendar() {
        return cal_IdCalSchool;
    }
    public void setIdCalendar(int cal_IdCalSchool){
        this.cal_IdCalSchool = cal_IdCalSchool;
    }
    @Column(name="calendar", insertable = false, updatable = false)
    public String getCalendar() {
        return calendar;
    }
    public void setCalendar(String calendar){
        this.calendar = calendar;
    }
}
