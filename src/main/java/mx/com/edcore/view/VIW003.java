/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de aspirantes
 * @author Gabriel Cisneros Landeros
 * @version 1.0.0
 */
@Entity
@Table(name="VIW003")
public class VIW003 implements java.io.Serializable{
    
    private int safe_IdUser;
    private int user_IdApplicant;
    private String code;
    private String name;
    private String firstLast;
    private String secondLast;
    private int prog_IdProgram;
    private String program;
    private String turn;
    private String curp;
    private String status;
    private int cal_IdCalSchool;
    private String calendar;
    private String type;
    
    public VIW003(){}
    
    @Column(name="safe_IdUser", insertable = false, updatable = false)
    public int getIdUser() {
        return safe_IdUser;
    }
    public void setIdUser(int safe_IdUser){
        this.safe_IdUser = safe_IdUser;
    }
    @Id
    @Column(name="user_IdApplicant", insertable = false, updatable = false)
    public int getIdApplicant() {
        return user_IdApplicant;
    }
    public void setIdApplicant(int user_IdApplicant){
        this.user_IdApplicant = user_IdApplicant;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    @Column(name="prog_IdProgram", insertable = false, updatable = false)
    public int getIdProgram() {
        return prog_IdProgram;
    }
    public void setIdProgram(int prog_IdProgram){
        this.prog_IdProgram = prog_IdProgram;
    }
    @Column(name="program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program){
        this.program = program;
    }
    @Column(name="curp", insertable = false, updatable = false)
    public String getCurp() {
        return curp;
    }
    public void setCurp(String curp){
        this.curp = curp;
    }
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
    @Column(name="turn", insertable = false, updatable = false)
    public String getTurn() {
        return turn;
    }
    public void setTurn(String turn){
        this.turn = turn;
    }
    @Column(name="type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    @Column(name="cal_IdCalSchool", insertable = false, updatable = false)
    public int getIdCalendar() {
        return cal_IdCalSchool;
    }
    public void setIdCalendar(int cal_IdCalSchool){
        this.cal_IdCalSchool = cal_IdCalSchool;
    }
    @Column(name="calendar", insertable = false, updatable = false)
    public String getCalendar() {
        return calendar;
    }
    public void setCalendar(String calendar){
        this.calendar = calendar;
    }
}
