/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Objeto Vista de aspirantes
 * @author Gabriel Cisneros Landeros
 * @version 1.0.0
 */
@Entity
@Table(name="VIW008")
public class VIW008 implements java.io.Serializable{
    
   
    private int group_IdGroupSubject;
    private String grupo;
    private int prog_IdSubject;
    private int prog_IdPlan;
    private String subject;
    private String abbreviation;
    private int semester;
    private String firstLast;
    private String secondLast;
    private String name;
    private int cal_IdCalSchool;
    private int RIDLU;
    private String RLU;
    private String LU;
    private int RIDMA;
    private String RMA;
    private String MA;
    private int RIDMI;
    private String RMI;
    private String MI;
    private int RIDJU;
    private String RJU;
    private String JU;
    private int RIDVI;
    private String RVI;
    private String VI;
    private int RIDSA;
    private String RSA;
    private String SA;
   
    
    public VIW008(){}
    
    
    @Id
    @Column(name="group_IdGroupSubject", insertable = false, updatable = false)
    public int getIdGroupSubject() {
        return group_IdGroupSubject;
    }
    public void setIdGroupSubject(int group_IdGroupSubject){
        this.group_IdGroupSubject = group_IdGroupSubject;
    }
    
    @Column(name="group", insertable = false, updatable = false)
    public String getGroup() {
        return grupo;
    }
    public void setGroup(String grupo){
        this.grupo = grupo;
    }
    
    @Column(name="prog_IdSubject", insertable = false, updatable = false)
    public int getIdSubject() {
        return prog_IdSubject;
    }
    public void setIdSubject(int prog_IdSubject){
        this.prog_IdSubject = prog_IdSubject;
    }
    
    @Column(name="prog_IdPlan", insertable = false, updatable = false)
    public int getIdPlan() {
        return prog_IdPlan;
    }
    public void setIdPlan(int prog_IdPlan){
        this.prog_IdPlan = prog_IdPlan;
    }        
    
    @Column(name="subject", insertable = false, updatable = false)
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject){
        this.subject = subject;
    }
    @Column(name="abbreviation", insertable = false, updatable = false)
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation){
        this.abbreviation = abbreviation;
    }
     @Column(name="semester", insertable = false, updatable = false)
    public int getSemester() {
        return semester;
    }
    public void setSemester(int semester){
        this.semester = semester;
    }
    
    @Column(name="firstLast", insertable = false, updatable = false)
    public String getFirstLast() {
        return firstLast;
    }
    public void setFirstLast(String firstLast){
        this.firstLast = firstLast;
    }
    @Column(name="secondLast", insertable = false, updatable = false)
    public String getSecondLast() {
        return secondLast;
    }
    public void setSecondLast(String secondLast){
        this.secondLast = secondLast;
    }
    @Column(name="name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    
     @Column(name="cal_IdCalSchool", insertable = false, updatable = false)
    public int getIdCalSchool() {
        return cal_IdCalSchool;
    }
    public void setIdCalSchool(int cal_IdCalSchool){
        this.cal_IdCalSchool = cal_IdCalSchool;
    }       
    /*
    @Column(name="RIDLU", insertable = false, updatable = false)
    public int getRidlu() {
        return RIDLU;
    }
    public void setRidlu(int RIDLU){
        this.RIDLU = RIDLU;
    }  
    
    @Column(name="RLU", insertable = false, updatable = false)
    public String getRlu() {
        return RLU;
    }
    public void setRlu(String RLU){
        this.RLU = RLU;
    }     
    
    @Column(name="LU", insertable = false, updatable = false)
    public String getLu() {
        return LU;
    }
    public void setLu(String LU){
        this.LU = LU;
    } 
    
    @Column(name="RIDMA", insertable = false, updatable = false)
    public int getRidma() {
        return RIDMA;
    }
    public void setRidma(int RIDMA){
        this.RIDMA = RIDMA;
    }  
    
    @Column(name="RMA", insertable = false, updatable = false)
    public String getRma() {
        return RMA;
    }
    public void setRma(String RMA){
        this.RMA = RMA;
    } 
    @Column(name="MA", insertable = false, updatable = false)
    public String getMa() {
        return MA;
    }
    public void setMa(String MA){
        this.MA = MA;
    }
    @Column(name="RIDMI", insertable = false, updatable = false)
    public int getRidmi() {
        return RIDMI;
    }
    public void setRidmi(int RIDMI){
        this.RIDMI = RIDMI;
    }  
    @Column(name="RMI", insertable = false, updatable = false)
    public String getRmi() {
        return RMI;
    }
    public void setRmi(String RMI){
        this.RMI = RMI;
    }
    @Column(name="MI", insertable = false, updatable = false)
    public String getMi() {
        return MI;
    }
    public void setMi(String MI){
        this.MI = MI;
    }
    
    @Column(name="RJU", insertable = false, updatable = false)
    public String getRju() {
        return RJU;
    }
    public void setRju(String RJU){
        this.RJU = RJU;
    }
    @Column(name="JU", insertable = false, updatable = false)
    public String getJu() {
        return JU;
    }
    public void setJu(String JU){
        this.JU = JU;
    }
    
     @Column(name="RIDVI", insertable = false, updatable = false)
    public int getRidvi() {
        return RIDVI;
    }
    public void setRidvi(int RIDVI){
        this.RIDVI = RIDVI;
    }  
    @Column(name="RVI", insertable = false, updatable = false)
    public String getRvi() {
        return RVI;
    }
    public void setRvi(String RVI){
        this.RVI = RVI;
    }
    @Column(name="VI", insertable = false, updatable = false)
    public String getVi() {
        return VI;
    }
    public void setVi(String VI){
        this.VI = VI;
    }
       
    @Column(name="RIDSA", insertable = false, updatable = false)
    public int getRidsa() {
        return RIDSA;
    }
    public void setRidsa(int RIDSA){
        this.RIDSA = RIDSA;
    }  
    @Column(name="RSA", insertable = false, updatable = false)
    public String getRsa() {
        return RSA;
    }
    public void setRsa(String RSA){
        this.RSA = RSA;
    }
    @Column(name="SA", insertable = false, updatable = false)
    public String getSa() {
        return SA;
    }
    public void setSa(String SA){
        this.SA = SA;
    }  
    */
}


