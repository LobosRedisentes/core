/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table; 

@Entity
@Table(name="VIW046")
public class VIW046 implements java.io.Serializable{

    private Long id;
    private Long fromUser;
    private Long fromAccount;
    private String fromUserName;
    private String fromUserFirstLast;
    private String fromUserSecondLast;
    private String subject;
    private String body;
    private Date date;
    private Long toUser;
    private Long toAccount;
    private String toUserName;
    private String toUserFirstLast;
    private String toUserSecondLast;
    private String status;
    
    public VIW046(){}

    @Id
    @Column(name="id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="fromUser", insertable = false, updatable = false)
    public Long getFromUser() {
        return fromUser;
    }
    public void setFromUser(Long fromUser) {
        this.fromUser = fromUser;
    }
    @Column(name="fromAccount", insertable = false, updatable = false)
    public Long getFromAccount() {
        return fromAccount;
    }
    public void setFromAccount(Long fromAccount) {
        this.fromAccount = fromAccount;
    }

    @Column(name="fromUserName", insertable = false, updatable = false)
    public String getFromUserName() {
        return fromUserName;
    }
    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    @Column(name="fromUserFirstLast", insertable = false, updatable = false)
    public String getFromUserFirstLast() {
        return fromUserFirstLast;
    }
    public void setFromUserFirstLast(String fromUserFirstLast) {
        this.fromUserFirstLast = fromUserFirstLast;
    }

    @Column(name="fromUserSecondLast", insertable = false, updatable = false)
    public String getFromUserSecondLast() {
        return fromUserSecondLast;
    }
    public void setFromUserSecondLast(String fromUserSecondLast) {
        this.fromUserSecondLast = fromUserSecondLast;
    }

    @Column(name="subject", insertable = false, updatable = false)
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Column(name="body", insertable = false, updatable = false)
    public String getBody() {
        return body;
    }
    public void setBody(String body) {
        this.body = body;
    }

    @Column(name="date", insertable = false, updatable = false)
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    @Column(name="toUser", insertable = false, updatable = false)
    public Long getToUser() {
        return toUser;
    }
    public void setToUser(Long toUser) {
        this.toUser = toUser;
    }
    @Column(name="toAccount", insertable = false, updatable = false)
    public Long getToAccount() {
        return toAccount;
    }
    public void setToAccount(Long toAccount) {
        this.toAccount = toAccount;
    }

    @Column(name="toUserName", insertable = false, updatable = false)
    public String getToUserName() {
        return toUserName;
    }
    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    @Column(name="toUserFirstLast", insertable = false, updatable = false)
    public String getToUserFirstLast() {
        return toUserFirstLast;
    }
    public void setToUserFirstLast(String toUserFirstLast) {
        this.toUserFirstLast = toUserFirstLast;
    }

    @Column(name="toUserSecondLast", insertable = false, updatable = false)
    public String getToUserSecondLast() {
        return toUserSecondLast;
    }
    public void setToUserSecondLast(String toUserSecondLast) {
        this.toUserSecondLast = toUserSecondLast;
    }

    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}