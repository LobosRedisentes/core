/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIW059")
public class VIW059 implements Serializable {

    private Long id;
    private String code;
    private String revoe;
    private String dgp;
    private String abbreviation;
    private String name;
    private String slogan;
    private String url;
    private String key;
    private String country;
    private String state;
    private String town;
    private String location;
    private String postCode;
    private String street;
    private String numExternal;
    private String numInternal;
    private String phone;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "revoe", insertable = false, updatable = false)
    public String getRevoe() {
        return revoe;
    }
    public void setRevoe(String revoe) {
        this.revoe = revoe;
    }

    @Column(name = "dgp", insertable = false, updatable = false)
    public String getDgp() {
        return dgp;
    }
    public void setDgp(String dgp) {
        this.dgp = dgp;
    }

    @Column(name = "abbreviation", insertable = false, updatable = false)
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Column(name = "name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "slogan", insertable = false, updatable = false)
    public String getSlogan() {
        return slogan;
    }
    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Column(name = "url", insertable = false, updatable = false)
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "key", insertable = false, updatable = false)
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    @Column(name = "country", insertable = false, updatable = false)
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "state", insertable = false, updatable = false)
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "town", insertable = false, updatable = false)
    public String getTown() {
        return town;
    }
    public void setTown(String town) {
        this.town = town;
    }
    
    @Column(name = "location", insertable = false, updatable = false)
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    @Column(name = "postCode", insertable = false, updatable = false)
    public String getPostCode() {
        return postCode;
    }
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "street", insertable = false, updatable = false)
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "numExternal", insertable = false, updatable = false)
    public String getNumExternal() {
        return numExternal;
    }
    public void setNumExternal(String numExternal) {
        this.numExternal = numExternal;
    }

    @Column(name = "numInternal", insertable = false, updatable = false)
    public String getNumInternal() {
        return numInternal;
    }
    public void setNumInternal(String numInternal) {
        this.numInternal = numInternal;
    }

    @Column(name = "phone", insertable = false, updatable = false)
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
