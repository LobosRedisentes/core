/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIW055")
public class VIW055 implements Serializable {

    private Long id;
    private String code;
    private String abbreviation;
    private String name;
    private String start;
    private String end;
    private String year;
    private String startVacation;
    private String endVacation;
    private String type;
    private String status;
    private Long office;
    private String officeName;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "id", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "abbreviation", insertable = false, updatable = false)
    public String getAbbreviation() {
        return abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    @Column(name = "name", insertable = false, updatable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "start", insertable = false, updatable = false)
    public String getStart() {
        return start;
    }
    public void setStart(String start) {
        this.start = start;
    }

    @Column(name = "end", insertable = false, updatable = false)
    public String getEnd() {
        return end;
    }
    public void setEnd(String end) {
        this.end = end;
    }

    @Column(name = "year", insertable = false, updatable = false)
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }

    @Column(name = "startVacation", insertable = false, updatable = false)
    public String getStartVacation() {
        return startVacation;
    }
    public void setStartVacation(String startVacation) {
        this.startVacation = startVacation;
    }

    @Column(name = "endVacation", insertable = false, updatable = false)
    public String getEndVacation() {
        return endVacation;
    }
    public void setEndVacation(String endVacation) {
        this.endVacation = endVacation;
    }

    @Column(name = "type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "office", insertable = false, updatable = false)
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }

    @Column(name = "officeName", insertable = false, updatable = false)
    public String getOfficeName() {
        return officeName;
    }
    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }
}
