/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table; 
/**
 *
 * @author wero
 */
@Entity
@Table(name="VIW013")
public class VIW013 implements java.io.Serializable{
    
private int safe_IdNotification;
    private int safe_IdAccountTrans;
    private String transmitter;
    private int safe_IdAccountRec;
    private String receiver;
    private String issue;
    private String content;
    private String date;
    private String status;
    
    
    public VIW013(){}
    
    @Column(name="safe_IdNotification", insertable = false, updatable = false)
    public int getIdNotification() {
        return safe_IdNotification;
    }
    public void setIdNotification(int safe_IdNotification){
        this.safe_IdNotification = safe_IdNotification;
    }
    @Id
    @Column(name="safe_IdAccountTrans", insertable = false, updatable = false)
    public int getIdAccountTrans() {
        return safe_IdAccountTrans;
    }
    public void setIdAccountTrans(int safe_IdAccountTrans){
        this.safe_IdAccountTrans = safe_IdAccountTrans;
    }
    @Column(name="transmitter", insertable = false, updatable = false)
    public String getTransmitter() {
        return transmitter;
    }
    public void setTransmitter(String transmitter){
        this.transmitter = transmitter;
    }
    @Column(name="safe_IdAccountRec", insertable = false, updatable = false)
    public int getIdAccountRec() {
        return safe_IdAccountRec;
    }
    public void setIdAccountRec(int safe_IdAccountRec){
        this.safe_IdAccountRec = safe_IdAccountRec;
    }
    @Column(name="receiver", insertable = false, updatable = false)
    public String getReceiver() {
        return receiver;
    }
    public void setReceiver(String receiver){
        this.receiver = receiver;
    }
    @Column(name="issue", insertable = false, updatable = false)
    public String getIssue() {
        return issue;
    }
    public void setIssue(String issue){
        this.issue = issue;
    }
    @Column(name="content", insertable = false, updatable = false)
    public String getContent() {
        return content;
    }
     public void setContent(String content){
        this.content = content;
    }
    public void setIdProgram(String content){
        this.content = content;
    }
    @Column(name="date", insertable = false, updatable = false)
    public String getDate() {
        return date;
    }
    public void setDate(String date){
        this.date = date;
    }
    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status){
        this.status = status;
    }
}