/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VAL001")
public class VAL001 implements Serializable {

    private Long id;
    private Long office;
    private String program;
    private String calendar;
    private String sex;
    private Long students;
    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "office", insertable = false, updatable = false)
    public Long getOffice() {
        return office;
    }
    public void setOffice(Long office) {
        this.office = office;
    }

    @Column(name = "program", insertable = false, updatable = false)
    public String getProgram() {
        return program;
    }
    public void setProgram(String program) {
        this.program = program;
    }

    @Column(name = "calendar", insertable = false, updatable = false)
    public String getCalendar() {
        return calendar;
    }
    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    @Column(name = "sex", insertable = false, updatable = false)
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "students", insertable = false, updatable = false)
    public Long getStudents() {
        return students;
    }
    public void setStudents(Long students) {
        this.students = students;
    }
}
