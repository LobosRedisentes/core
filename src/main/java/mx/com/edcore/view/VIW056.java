/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIW056")
public class VIW056 implements Serializable {

    private Long id;
    private Long country;
    private String countryName;
    private Long state;
    private String stateName;
    private Long town;
    private String townName;
    private Long location;
    private String locationName;
    private String postCode;
    private String street;
    private String numExternal;
    private String numInternal;
    private String phone;
    private String cellPhone;

    @Id
    @Column(name = "id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "country", insertable = false, updatable = false)
    public Long getCountry() {
        return country;
    }
    public void setCountry(Long country) {
        this.country = country;
    }

    @Column(name = "countryName", insertable = false, updatable = false)
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Column(name = "state", insertable = false, updatable = false)
    public Long getState() {
        return state;
    }
    public void setState(Long state) {
        this.state = state;
    }

    @Column(name = "stateName", insertable = false, updatable = false)
    public String getStateName() {
        return stateName;
    }
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Column(name = "town", insertable = false, updatable = false)
    public Long getTown() {
        return town;
    }
    public void setTown(Long town) {
        this.town = town;
    }

    @Column(name = "townName", insertable = false, updatable = false)
    public String getTownName() {
        return townName;
    }
    public void setTownName(String townName) {
        this.townName = townName;
    }

    @Column(name = "location", insertable = false, updatable = false)
    public Long getLocation() {
        return location;
    }
    public void setLocation(Long location) {
        this.location = location;
    }

    @Column(name = "locationName", insertable = false, updatable = false)
    public String getLocationName() {
        return locationName;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Column(name = "postCode", insertable = false, updatable = false)
    public String getPostCode() {
        return postCode;
    }
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Column(name = "street", insertable = false, updatable = false)
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    @Column(name = "numExternal", insertable = false, updatable = false)
    public String getNumExternal() {
        return numExternal;
    }
    public void setNumExternal(String numExternal) {
        this.numExternal = numExternal;
    }

    @Column(name = "numInternal", insertable = false, updatable = false)
    public String getNumInternal() {
        return numInternal;
    }
    public void setNumInternal(String numInternal) {
        this.numInternal = numInternal;
    }

    @Column(name = "phone", insertable = false, updatable = false)
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "cellPhone", insertable = false, updatable = false)
    public String getCellPhone() {
        return cellPhone;
    }
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }
}
