/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author wero
 */
@Entity
@Table(name="VIW005")
public class VIW005 implements java.io.Serializable{
    
private int conv_IdSubject;
    private int conv_IdResolution;
    private String code;
    private String subject;
    private int credits;
    private int hrsPractices;
    private int hrsTheorist;
    private String plan;
    private int semester;
    private int grade;
    
    
    public VIW005(){}
    
    @Column(name="conv_IdSubject", insertable = false, updatable = false)
    public int getIdSubject() {
        return conv_IdSubject;
    }
    public void setIdSubject(int conv_IdSubject){
        this.conv_IdSubject = conv_IdSubject;
    }
    @Id
    @Column(name="conv_IdResolution", insertable = false, updatable = false)
    public int getIdResolution() {
        return conv_IdResolution;
    }
    public void setIdResolution(int conv_IdResolution){
        this.conv_IdResolution = conv_IdResolution;
    }
    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    @Column(name="subject", insertable = false, updatable = false)
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject){
        this.subject = subject;
    }
    @Column(name="credits", insertable = false, updatable = false)
    public int getCredits() {
        return credits;
    }
    public void setCredits(int credits){
        this.credits = credits;
    }
    @Column(name="hrsPractices", insertable = false, updatable = false)
    public int getHrsPractices() {
        return hrsPractices;
    }
    public void setHrsPractices(int hrsPractices){
        this.hrsPractices = hrsPractices;
    }
    @Column(name="hrsTheorist", insertable = false, updatable = false)
    public int getHrsTheorist() {
        return hrsTheorist;
    }
    public void setHrsTheorist(int hrsTheorist){
        this.hrsTheorist = hrsTheorist;
    }
    @Column(name="plan", insertable = false, updatable = false)
    public String getPlan() {
        return plan;
    }
    public void setPlan(String plan){
        this.plan = plan;
    }
    @Column(name="semester", insertable = false, updatable = false)
    public int getSemester() {
        return semester;
    }
    public void setSemester(int semester){
        this.semester = semester;
    }
    @Column(name="grade", insertable = false, updatable = false)
    public int getGrade() {
        return grade;
    }
    public void setGrade(int grade){
        this.grade = grade;
    }
    
 }

