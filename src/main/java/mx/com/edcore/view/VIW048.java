/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.edcore.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table; 
/**
 * Lista de privilegios
 * 
 */
@Entity
@Table(name="VIW048")
public class VIW048 implements java.io.Serializable{

    private Long id;
    private Long operation;
    private String code;
    private String type;
    private String platform;
    private Long rol;
    private String rolName;
    private String rolType;
    private String status;

    
    public VIW048(){}

    @Id
    @Column(name="id", insertable = false, updatable = false)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @Column(name="operation", insertable = false, updatable = false)
    public Long getOperation() {
        return operation;
    }
    public void setOperation(Long safe_IdOperation) {
        this.operation = safe_IdOperation;
    }

    @Column(name="code", insertable = false, updatable = false)
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="type", insertable = false, updatable = false)
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    @Column(name="platform", insertable = false, updatable = false)
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Column(name="rol", insertable = false, updatable = false)
    public Long getRol() {
        return rol;
    }
    public void setRol(Long safe_IdRol) {
        this.rol = safe_IdRol;
    }

    @Column(name="rolName", insertable = false, updatable = false)
    public String getRolName() {
        return rolName;
    }
    public void setRolName(String rolName) {
        this.rolName = rolName;
    }

    @Column(name="rolType", insertable = false, updatable = false)
    public String getRolType() {
        return rolType;
    }
    public void setRolType(String rolType) {
        this.rolType = rolType;
    }   

    @Column(name="status", insertable = false, updatable = false)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}